#!/bin/bash -li
# shellcheck disable=SC1091
set -e
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

. "$SCRIPTDIR/test_vers.sh"

log() {
    echo "$*" >&2
}

die() {
    log "${1:-}"
    exit "${2-1}"
}

test_in_env() {
    local env_dir
    env_dir="$1"
    log "--------Testing in $env_dir--------"
    . "$env_dir/bin/activate"
    cd "$SCRIPTDIR/.." || die "couldn't cd"
    log "----pytest----"
    pytest
    log "----mypy----"
    mypy ./ --cache-dir "$env_dir/.mypy_cache"
    deactivate
    log "--------DONE with $env_dir--------"
    echo -e "\n\n\n" >&2
}

for py_ver in "${PY_VERS[@]}"; do
    testbed_dir="$SCRIPTDIR/testenvs/py${py_ver}"
    test_in_env "$testbed_dir"
done
